#!/bin/sh -eux
# install-ntpd.sh -- to keep the machine running on time
# Copyright (C) 2017  Olaf Meeuwissen
#
# License: GPL-3.0+

apt-get install --quiet --assume-yes \
        openntpd

if test -n "$ntp_server"; then
    sed -i \
        -e "s|^servers|#servers|" \
        -e "/^#server /s|.*|server $ntp_server|" \
        /etc/openntpd/ntpd.conf
    systemctl restart openntpd
fi
