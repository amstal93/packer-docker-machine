Packer Docker Machine
=====================

Build a Debian-based Docker Machine without user interaction.

 - all available security upgrades will be applied
 - non-necessary packages will be purged
 - recommended packages are *not* automatically installed
 - the `root` account will be disabled, use `sudo`

You can login with the `username` and `password` defined near the top
of the [Makefile](Makefile).

Requirements
------------
 - `make`
 - `packer`
 - `virtualbox`

Usage
-----
```sh
make
```
The build will take a while but after your `docker-machine` has been
created, you can start it with
```sh
vbox=$(basename output-docker-machine/packer-docker-machine-*.ovf .ovf)
VBoxManage import output-docker-machine/$vbox.ovf
VBoxManage startvm $vbox
```
You can also use the VirtualBox GUI to import and start the machine.
